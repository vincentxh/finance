package integration.value;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vnx.finance.value.controller.ValueController;
import com.vnx.finance.value.controller.data.outbound.StockSummaryRest;
import com.vnx.finance.value.service.data.StockSummary;
import com.vnx.finance.value.service.implementation.ValueServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = ValueController.class)
@WebMvcTest(controllers = ValueController.class)
public class ValueControllerIntegrationTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@MockBean
	private ValueServiceImpl valueService;

	@Test
	public void test_stockById() throws Exception {

		final var summary = mock(TestStockSummary.class);
		when(summary.getCompanyName()).thenReturn("Apple");

		when(valueService.stockSummaryBySymbol("aapl")).thenReturn(summary);

		final var result = mockMvc.perform(get("/api/v0/stock/{symbol}", "aapl")
								.contentType(MediaType.APPLICATION_JSON))
			   .andExpect(status().isOk())
			   .andExpect(content().contentType(MediaType.APPLICATION_JSON))
							.andReturn()
							.getResponse();

		final var dto = objectMapper.readValue(result.getContentAsByteArray(), StockSummaryRest.class);

		assertNotNull(dto);
		assertEquals("Apple", dto.getCompanyName());

	}



}

class TestStockSummary implements StockSummary {

	@Override
	public String getSymbol() {
		return null;
	}

	@Override
	public String getCompanyName() {
		return null;
	}

	@Override
	public double getTrailingPE() {
		return 0;
	}

	@Override
	public double getPricePreviousClosing() {
		return 0;
	}

	@Override
	public double getDE() {
		return 0;
	}

	@Override
	public double getPS() {
		return 0;
	}

	@Override
	public double getTrailingDividendRate() {
		return 0;
	}
}
