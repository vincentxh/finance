package integration.greet;

import com.vnx.finance.greet.controller.GreetController;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = GreetController.class)
@WebMvcTest(controllers = GreetController.class)
public class GreetControllerIntegrationTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void test_hello() throws Exception {

		mockMvc.perform(get("/greet/hello")
								.contentType(MediaType.TEXT_PLAIN_VALUE))
			   .andExpect(content().string("hello"))
			   .andExpect(status().isOk());

	}
}
