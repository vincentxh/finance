package com.vnx.finance.compound;


import com.vnx.finance.util.InterestUtil;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BasicCompounderTest {

	@Test
	void test_calculateCompoundedLumpSum_afterNoStep() {

		final var basicCompounder = mock(BasicCompounder.class);
		when(basicCompounder.calculateCompoundedLumpSum(anyInt(), any(), any())).thenCallRealMethod();

		final var result = basicCompounder.calculateCompoundedLumpSum(0, BigDecimal.valueOf(1.1), BigDecimal.valueOf(100));

		assertEquals(100, result.intValue());
	}

	@Test
	void test_calculateCompoundedLumpSum_afterFirstStep() {


		final var basicCompounder = mock(BasicCompounder.class);
		when(basicCompounder.calculateCompoundedLumpSum(anyInt(), any(), any())).thenCallRealMethod();

		final var result = basicCompounder.calculateCompoundedLumpSum(1, BigDecimal.valueOf(1.1), BigDecimal.valueOf(100));

		assertEquals(110, result.intValue());
	}

	@Test
	void test_calculateCompoundedLumpSum_afterSecondStep() {

		final var basicCompounder = mock(BasicCompounder.class);
		when(basicCompounder.calculateCompoundedLumpSum(anyInt(), any(), any())).thenCallRealMethod();

		final var result = basicCompounder.calculateCompoundedLumpSum(2, BigDecimal.valueOf(1.1), BigDecimal.valueOf(100));

		assertEquals(121, result.intValue());
	}

	@Test
	void test_calculateCompoundedDeposits_afterNoStep() {

		final var basicCompounder = mock(BasicCompounder.class);
		when(basicCompounder.calculateCompoundedDeposits(anyInt(), any(), any())).thenCallRealMethod();

		final var result = basicCompounder.calculateCompoundedDeposits(0, BigDecimal.valueOf(1.1), BigDecimal.valueOf(100));

		assertEquals(0, result.intValue());
	}

	@Test
	void test_calculateCompoundedDeposits_afterFirstStep() {

		final var basicCompounder = mock(BasicCompounder.class);
		when(basicCompounder.calculateCompoundedDeposits(anyInt(), any(), any())).thenCallRealMethod();

		final var result = basicCompounder.calculateCompoundedDeposits(1, BigDecimal.valueOf(1.1), BigDecimal.valueOf(100));

		assertEquals(110, result.intValue());
	}

	@Test
	void test_calculateCompoundedDeposits_afterSecondStep() {

		final var basicCompounder = mock(BasicCompounder.class);
		when(basicCompounder.calculateCompoundedDeposits(anyInt(), any(), any())).thenCallRealMethod();

		final var result = basicCompounder.calculateCompoundedDeposits(2, BigDecimal.valueOf(1.1), BigDecimal.valueOf(100));

		assertEquals(231, result.intValue());
	}
}