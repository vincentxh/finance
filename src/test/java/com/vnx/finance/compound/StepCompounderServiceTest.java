package com.vnx.finance.compound;

import com.vnx.finance.compound.service.data.CompoundingSchedule;
import com.vnx.finance.compound.service.data.CompoundingStep;
import com.vnx.finance.compound.service.StepCompounderService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class StepCompounderServiceTest {

	@Test
	void test_compound() {

		final var stepCompounder = mock(StepCompounderService.class);
		when(stepCompounder.compound(any())).thenCallRealMethod();
		when(stepCompounder._depositAndCompound(any(), any(), any())).thenCallRealMethod();

		final var schedule = new TestCompoundingSchedule(
				BigDecimal.valueOf(100),
				List.of(new TestCompoundingStep(BigDecimal.valueOf(1.1),
				  								 BigDecimal.valueOf(100)))
		);

		final var result = stepCompounder.compound(schedule);

		assertEquals(2, result.size());

		final var firstStep = result.get(0);
		assertEquals(BigDecimal.ONE, firstStep.getCompoundingStepMultiplier());
		assertEquals(100, firstStep.getDepositForStep().intValue());
		assertEquals(100, firstStep.getBalanceAfterStep().intValue());

		final var secondStep = result.get(1);
		assertEquals(BigDecimal.valueOf(1.1), secondStep.getCompoundingStepMultiplier());
		assertEquals(100, secondStep.getDepositForStep().intValue());
		assertEquals(220, secondStep.getBalanceAfterStep().intValue());
	}

	@Test
	void test_compound_onlyLumpSum() {

		final var stepCompounder = mock(StepCompounderService.class);
		when(stepCompounder.compound(any())).thenCallRealMethod();

		final var schedule = new TestCompoundingSchedule(BigDecimal.valueOf(100), List.of());

		final var result = stepCompounder.compound(schedule);

		assertEquals(1, result.size());

		final var step = result.get(0);
		assertEquals(BigDecimal.ONE, step.getCompoundingStepMultiplier());
		assertEquals(100, step.getDepositForStep().intValue());
		assertEquals(100, step.getBalanceAfterStep().intValue());
	}

	@Test
	void test_depositAndCompound() {

		final var stepCompounder = mock(StepCompounderService.class);
		when(stepCompounder._depositAndCompound(any(), any(), any())).thenCallRealMethod();

		final var result = stepCompounder._depositAndCompound(BigDecimal.valueOf(100),
															  BigDecimal.valueOf(100),
															  BigDecimal.valueOf(1.1));

		assertEquals(220, result.intValue());
	}

	@Test
	void test_depositAndCompound_forZeroBalance() {

		final var stepCompounder = mock(StepCompounderService.class);
		when(stepCompounder._depositAndCompound(any(), any(), any())).thenCallRealMethod();

		final var result = stepCompounder._depositAndCompound(BigDecimal.ZERO,
															  BigDecimal.valueOf(100),
															  BigDecimal.valueOf(1.1));

		assertEquals(110, result.intValue());
	}

	@Test
	void test_depositAndCompound_forZeroDeposit() {

		final var stepCompounder = mock(StepCompounderService.class);
		when(stepCompounder._depositAndCompound(any(), any(), any())).thenCallRealMethod();

		final var result = stepCompounder._depositAndCompound(BigDecimal.valueOf(100),
															  BigDecimal.ZERO,
															  BigDecimal.valueOf(1.1));

		assertEquals(110, result.intValue());
	}

	@Getter
	@RequiredArgsConstructor
	private class TestCompoundingSchedule implements CompoundingSchedule {

		private final BigDecimal lumpSum;
		private final List<CompoundingStep> compoundingSteps;

	}

	@Getter
	@RequiredArgsConstructor
	private class TestCompoundingStep implements CompoundingStep {

		private final BigDecimal compoundingStepMultiplier;
		private final BigDecimal depositForStep;

	}

}


