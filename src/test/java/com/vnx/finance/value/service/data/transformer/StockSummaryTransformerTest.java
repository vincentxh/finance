package com.vnx.finance.value.service.data.transformer;

import com.vnx.finance.value.client.data.StockSummaryDTO;
import com.vnx.finance.value.service.data.StockSummary;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StockSummaryTransformerTest {

	@Test
	void test_stockSummaryFrom() {

		final var summaryDTO = mock(TestStockSummaryDTO.class);
		when(summaryDTO.getSymbol()).thenReturn("symbol");
		when(summaryDTO.getCompanyName()).thenReturn("companyName");
		when(summaryDTO.getTrailingPE()).thenReturn(1d);
		when(summaryDTO.getPricePreviousClosing()).thenReturn(2d);
		when(summaryDTO.getDE()).thenReturn(3d);
		when(summaryDTO.getPS()).thenReturn(4d);
		when(summaryDTO.getTrailingDividendRate()).thenReturn(5d);

		final var transformer = mock(StockSummaryTransformer.class);
		when(transformer.stockSummaryFrom(summaryDTO)).thenCallRealMethod();

		final StockSummary stockSummary = transformer.stockSummaryFrom(summaryDTO);
		assertNotNull(stockSummary);

		assertEquals("symbol", stockSummary.getSymbol());
		assertEquals("companyName", stockSummary.getCompanyName());
		assertEquals(1, stockSummary.getTrailingPE());
		assertEquals(2, stockSummary.getPricePreviousClosing());
		assertEquals(3, stockSummary.getDE());
		assertEquals(4, stockSummary.getPS());
		assertEquals(5, stockSummary.getTrailingDividendRate());
	}
}

class TestStockSummaryDTO implements StockSummaryDTO {

	@Override
	public String getSymbol() {
		return null;
	}

	@Override
	public String getCompanyName() {
		return null;
	}

	@Override
	public double getTrailingPE() {
		return 0;
	}

	@Override
	public double getPricePreviousClosing() {
		return 0;
	}

	@Override
	public double getDE() {
		return 0;
	}

	@Override
	public double getPS() {
		return 0;
	}

	@Override
	public double getTrailingDividendRate() {
		return 0;
	}
}

