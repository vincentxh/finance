package com.vnx.finance.value.client.iex.data;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class IEXStockSummaryDTOTest {

	@Test
	void test_from() {

		final var quote = mock(IEXStockQuoteDTO.class);
		when(quote.getSymbol()).thenReturn("symbol");
		when(quote.getCompanyName()).thenReturn("companyName");
		when(quote.getPeRatio()).thenReturn(1d);
		when(quote.getPreviousClose()).thenReturn(2d);

		final var stats = mock(IEXStockAdvancedStatsDTO.class);
		when(stats.getDebtToEquity()).thenReturn(3d);
		when(stats.getPriceToSales()).thenReturn(4d);

		final double trailingDividendRate = 42;

		final var result = IEXStockSummaryDTO.from(quote, stats, trailingDividendRate);
		assertNotNull(result);
		assertEquals("symbol", result.getSymbol());
		assertEquals("companyName", result.getCompanyName());
		assertEquals(1, result.getTrailingPE());
		assertEquals(2, result.getPricePreviousClosing());
		assertEquals(3, result.getDE());
		assertEquals(4, result.getPS());
		assertEquals(42, result.getTrailingDividendRate());

	}
}