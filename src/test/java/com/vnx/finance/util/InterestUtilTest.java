package com.vnx.finance.util;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

class InterestUtilTest {

	private static final double ACCEPTED_DELTA = 0.0000001;

	@Test
	void test_monthlyMultiplier_accuracy() {

		final var yearlyRate = BigDecimal.valueOf(.0555555);

		assertEquals(1 + yearlyRate.doubleValue(), InterestUtil.monthlyMultiplier(yearlyRate).pow(12).doubleValue(),
					 ACCEPTED_DELTA, "Not accurate to 10^-7");

	}

	@Test
	void test_monthlyMultiplier() {

		final var result = InterestUtil.monthlyMultiplier(BigDecimal.valueOf(4095));
		assertEquals(0, result.compareTo(BigDecimal.valueOf(2)));
	}

	@Test
	void test_monthlyInterestRate_accuracy() {

		final var yearlyRate = BigDecimal.valueOf(.0555555);

		assertEquals(yearlyRate.doubleValue(),
					 InterestUtil.monthlyInterestRate(yearlyRate)
								 .add(BigDecimal.ONE)
								 .pow(12)
								 .subtract(BigDecimal.ONE)
								 .doubleValue(),
					 ACCEPTED_DELTA, "Not accurate to 10^-7");

	}

	@Test
	void test_monthlyInterestRate() {

		final var result = InterestUtil.monthlyInterestRate(BigDecimal.valueOf(4095));
		assertEquals(0, result.compareTo(BigDecimal.ONE));
	}

	@Test
	void test_calculateSubPeriodMultiplier() {
		final var result = InterestUtil.calculateSubPeriodMultiplier(BigDecimal.valueOf(3), 2);
		assertEquals(0, result.compareTo(BigDecimal.valueOf(2)));
	}

	@Test
	void test_calculateSubPeriodInterestRate() {

		final var result = InterestUtil.calculateSubPeriodInterestRate(BigDecimal.valueOf(3), 2);
		assertEquals(0, result.compareTo(BigDecimal.ONE));
	}
}