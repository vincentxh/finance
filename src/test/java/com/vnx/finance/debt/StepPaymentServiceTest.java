package com.vnx.finance.debt;

import com.vnx.finance.debt.data.DebtPaymentSchedule;
import com.vnx.finance.debt.data.PaymentStep;
import com.vnx.finance.debt.data.PaymentStepResult;
import com.vnx.finance.debt.data.PaymentType;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Test;

import javax.validation.constraints.NotNull;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class StepPaymentServiceTest {

	@Test
	void test_createDebtPaymentOverview() {

		final PaymentStep paymentStep = mock(TestPaymentStep.class);
		when(paymentStep.getPaymentType()).thenReturn(PaymentType.ANNUITY);
		when(paymentStep.getInterestRateForStep()).thenReturn(BigDecimal.TEN);
		when(paymentStep.getExtraPrincipalPayment()).thenReturn(BigDecimal.TEN);
		when(paymentStep.getInterestRateForStep()).thenReturn(BigDecimal.TEN);

		final PaymentStepResult paymentStepResult = mock(TestPaymentStepResult.class);
		when(paymentStepResult.getPrincipalAfterPayment()).thenReturn(BigDecimal.ZERO);

		final List<PaymentStep> paymentSteps = List.of(paymentStep, paymentStep, paymentStep);
		final var principal = BigDecimal.ONE;

		final DebtPaymentSchedule paymentSchedule = mock(TestDebtPaymentSchedule.class);
		when(paymentSchedule.getPaymentSteps()).thenReturn(paymentSteps);
		when(paymentSchedule.getPrincipal()).thenReturn(principal);

		final StepPaymentService stepPaymentService = mock(StepPaymentService.class);
		when(stepPaymentService.createDebtPaymentOverview(any())).thenCallRealMethod();
		when(stepPaymentService.buildPaymentStep(any(), any(), anyInt())).thenReturn(paymentStepResult);

		final var result = stepPaymentService.createDebtPaymentOverview(paymentSchedule);

		verify(stepPaymentService, times(3)).buildPaymentStep(any(), any(), anyInt());

		assertNotNull(result);
		assertEquals(principal, result.getPrincipal());
		assertEquals(3, result.getPaymentOverview().size());
	}

	@Test
	void test_buildPaymentStep_composesPaymentStepResult() {

		final var principal = BigDecimal.valueOf(2);
		final var paymentsLeft = 42;

		final PaymentStep paymentStep = mock(TestPaymentStep.class);
		when(paymentStep.getPaymentType()).thenReturn(PaymentType.ANNUITY);
		when(paymentStep.getInterestRateForStep()).thenReturn(BigDecimal.TEN);
		when(paymentStep.getExtraPrincipalPayment()).thenReturn(BigDecimal.TEN);
		when(paymentStep.getInterestRateForStep()).thenReturn(BigDecimal.TEN);

		final StepPaymentService stepPaymentService = mock(StepPaymentService.class);
		when(stepPaymentService.buildPaymentStep(paymentStep, principal, paymentsLeft)).thenCallRealMethod();

		when(stepPaymentService.calculateInterestPayment(any(), any())).thenReturn(BigDecimal.ZERO);
		when(stepPaymentService.calculatePrincipalPayment(any(), anyInt(), any())).thenReturn(BigDecimal.ZERO);
		when(stepPaymentService.calculateFullPayment(any(), anyInt(), any())).thenReturn(BigDecimal.ZERO);

		final var result = stepPaymentService.buildPaymentStep(paymentStep, principal, paymentsLeft);

		assertNotNull(result);

		assertEquals(PaymentType.ANNUITY, result.getPaymentType());
		assertEquals(BigDecimal.TEN ,result.getInterestRateForStep());
		assertEquals(BigDecimal.TEN ,result.getExtraPrincipalPayment());

		assertEquals(BigDecimal.ZERO,result.getInterestPayment());
		assertEquals(BigDecimal.ZERO,result.getBasePrincipalPayment());
		assertEquals(BigDecimal.ZERO,result.getFullPayment());

		assertNotNull(result.getPrincipalAfterPayment());

	}

	@Test
	void test_buildPaymentStep_calculatesPaymentElements() {

		final var principal = BigDecimal.valueOf(2);
		final var paymentsLeft = 42;

		final PaymentStep paymentStep = mock(TestPaymentStep.class);
		when(paymentStep.getInterestRateForStep()).thenReturn(BigDecimal.TEN);
		when(paymentStep.getExtraPrincipalPayment()).thenReturn(BigDecimal.TEN);

		final StepPaymentService stepPaymentService = mock(StepPaymentService.class);
		when(stepPaymentService.buildPaymentStep(paymentStep, principal, paymentsLeft)).thenCallRealMethod();

		when(stepPaymentService.calculateInterestPayment(any(), any())).thenReturn(BigDecimal.ZERO);
		when(stepPaymentService.calculatePrincipalPayment(any(), anyInt(), any())).thenReturn(BigDecimal.ZERO);
		when(stepPaymentService.calculateFullPayment(any(), anyInt(), any())).thenReturn(BigDecimal.ZERO);

		stepPaymentService.buildPaymentStep(paymentStep, principal, paymentsLeft);

		verify(stepPaymentService).calculateInterestPayment(any(), any());
		verify(stepPaymentService).calculatePrincipalPayment(any(), anyInt(), any());
		verify(stepPaymentService).calculateFullPayment(any(), anyInt(), any());
	}

	@Test
	void test_buildPaymentStep_reducesPrincipal() {

		final var principal = BigDecimal.valueOf(2);
		final var paymentsLeft = 42;

		final PaymentStep paymentStep = mock(TestPaymentStep.class);
		when(paymentStep.getInterestRateForStep()).thenReturn(BigDecimal.TEN);


		final StepPaymentService stepPaymentService = mock(StepPaymentService.class);
		when(stepPaymentService.buildPaymentStep(paymentStep, principal, paymentsLeft))
				.thenCallRealMethod();

		// subtracted
		when(stepPaymentService.calculatePrincipalPayment(principal, paymentsLeft, paymentStep)).thenReturn(BigDecimal.ONE);
		when(paymentStep.getExtraPrincipalPayment()).thenReturn(BigDecimal.ONE);

		final var result = stepPaymentService.buildPaymentStep(paymentStep, principal, paymentsLeft);

		assertEquals(0, result.getPrincipalAfterPayment().compareTo(BigDecimal.ZERO));
	}

	@Test
	void test_calculateInterestPayment() {

		final StepPaymentService stepPaymentService = mock(StepPaymentService.class);
		when(stepPaymentService.calculateInterestPayment(any(), any())).thenCallRealMethod();

		final var result = stepPaymentService.calculateInterestPayment(BigDecimal.TEN, BigDecimal.valueOf(.1));

		assertEquals(0, result.compareTo(BigDecimal.ONE));
	}

	@Test
	void test_calculateFullPayment_annuity() {

		final var testCase = PaymentType.ANNUITY;

		final var principal = BigDecimal.ZERO;
		final var periodsLeft = 0;

		final PaymentStep paymentStep = mock(TestPaymentStep.class);
		when(paymentStep.getPaymentType()).thenReturn(testCase);
		when(paymentStep.getInterestRateForStep()).thenReturn(BigDecimal.ZERO);

		final StepPaymentService stepPaymentService = mock(StepPaymentService.class);
		when(stepPaymentService.calculateFullPayment(principal, periodsLeft, paymentStep)).thenCallRealMethod();

		// return values should be added
		when(stepPaymentService.calculateAnnuityPayment(principal, periodsLeft, BigDecimal.ZERO)).thenReturn(BigDecimal.ONE);
		when(paymentStep.getExtraPrincipalPayment()).thenReturn(BigDecimal.ONE);

		final var result = stepPaymentService.calculateFullPayment(principal, periodsLeft, paymentStep);

		assertEquals(0, result.compareTo(BigDecimal.valueOf(2)));
	}

	@Test
	void test_calculateFullPayment_linear() {

		final var testCase = PaymentType.LINEAR;

		final var principal = BigDecimal.ZERO;
		final var periodsLeft = 0;

		final PaymentStep paymentStep = mock(TestPaymentStep.class);
		when(paymentStep.getPaymentType()).thenReturn(testCase);
		when(paymentStep.getInterestRateForStep()).thenReturn(BigDecimal.ZERO);

		final StepPaymentService stepPaymentService = mock(StepPaymentService.class);
		when(stepPaymentService.calculateFullPayment(principal, periodsLeft, paymentStep)).thenCallRealMethod();

		// return values should be added
		when(stepPaymentService.calculateLinearPrincipalPayment(principal, periodsLeft)).thenReturn(BigDecimal.ONE);
		when(stepPaymentService.calculateInterestPayment(principal, BigDecimal.ZERO)).thenReturn(BigDecimal.ONE);
		when(paymentStep.getExtraPrincipalPayment()).thenReturn(BigDecimal.ONE);

		final var result = stepPaymentService.calculateFullPayment(principal, periodsLeft, paymentStep);

		assertEquals(0, result.compareTo(BigDecimal.valueOf(3)));
	}

	@Test
	void test_calculateFullPayment_throwsWithNoPaymentType() {

		final PaymentType testCase = null;

		final var principal = BigDecimal.ZERO;
		final var periodsLeft = 0;

		final PaymentStep paymentStep = mock(TestPaymentStep.class);
		when(paymentStep.getPaymentType()).thenReturn(testCase);

		final StepPaymentService stepPaymentService = mock(StepPaymentService.class);
		when(stepPaymentService.calculateFullPayment(principal, periodsLeft, paymentStep)).thenCallRealMethod();

		assertThrows(NullPointerException.class, () -> stepPaymentService.calculateFullPayment(principal, periodsLeft, paymentStep));
	}

	@Test
	void test_calculatePrincipalPayment_calculatesAnnuity() {

		final var principal = BigDecimal.TEN;
		final var periods = 1;
		final var interestRate = BigDecimal.ONE;

		final var paymentStep = mock(PaymentStep.class);
		when(paymentStep.getPaymentType()).thenReturn(PaymentType.ANNUITY);
		when(paymentStep.getInterestRateForStep()).thenReturn(interestRate);

		final StepPaymentService stepPaymentService = mock(StepPaymentService.class);
		when(stepPaymentService.calculatePrincipalPayment(any(), anyInt(), any())).thenCallRealMethod();
		when(stepPaymentService.calculateAnnuityPrincipalPayment(principal, periods, interestRate)).thenReturn(BigDecimal.ZERO);


		final var result = stepPaymentService.calculatePrincipalPayment(principal, periods, paymentStep);
		assertEquals(0, result.compareTo(BigDecimal.ZERO));
	}

	@Test
	void test_calculatePrincipalPayment_calculatesLinear() {

		final var principal = BigDecimal.TEN;
		final var periods = 1;
		final var interestRate = BigDecimal.ONE;

		final var paymentStep = mock(PaymentStep.class);
		when(paymentStep.getPaymentType()).thenReturn(PaymentType.LINEAR);
		when(paymentStep.getInterestRateForStep()).thenReturn(interestRate);

		final StepPaymentService stepPaymentService = mock(StepPaymentService.class);
		when(stepPaymentService.calculatePrincipalPayment(any(), anyInt(), any())).thenCallRealMethod();
		when(stepPaymentService.calculateLinearPrincipalPayment(principal, periods)).thenReturn(BigDecimal.ZERO);


		final var result = stepPaymentService.calculatePrincipalPayment(principal, periods, paymentStep);
		assertEquals(0, result.compareTo(BigDecimal.ZERO));
	}

	@Test
	void test_calculatePrincipalPayment_throwsWithNoPaymentType() {

		final var paymentStep = mock(PaymentStep.class);
		when(paymentStep.getPaymentType()).thenReturn(null);

		final StepPaymentService stepPaymentService = mock(StepPaymentService.class);
		when(stepPaymentService.calculatePrincipalPayment(any(), anyInt(), any())).thenCallRealMethod();

		assertThrows(NullPointerException.class, () -> stepPaymentService.calculatePrincipalPayment(BigDecimal.ZERO, 0,
																									paymentStep));
	}

	@Test
	void test_calculateAnnuityPayment() {

		final StepPaymentService stepPaymentService = mock(StepPaymentService.class);
		when(stepPaymentService.calculateAnnuityPayment(any(), anyInt(), any())).thenCallRealMethod();

		final var result = stepPaymentService.calculateAnnuityPayment(BigDecimal.TEN, 1, BigDecimal.valueOf(.1));
		assertEquals(0, result.compareTo(BigDecimal.valueOf(11)));
	}

	@Test
	void test_calculateAnnuityPrincipalPayment() {

		final StepPaymentService stepPaymentService = mock(StepPaymentService.class);
		when(stepPaymentService.calculateAnnuityPrincipalPayment(any(), anyInt(), any())).thenCallRealMethod();
		when(stepPaymentService.calculateAnnuityPayment(any(), anyInt(), any())).thenCallRealMethod();
		when(stepPaymentService.calculateInterestPayment(any(), any())).thenCallRealMethod();


		final var result = stepPaymentService.calculateAnnuityPrincipalPayment(BigDecimal.TEN, 1, BigDecimal.valueOf(1.1));
		assertEquals(0, result.compareTo(BigDecimal.TEN));
	}

	@Test
	void test_calculateLinearPrincipalPayment() {

		final StepPaymentService stepPaymentService = mock(StepPaymentService.class);
		when(stepPaymentService.calculateLinearPrincipalPayment(any(), anyInt())).thenCallRealMethod();

		final var result = stepPaymentService.calculateLinearPrincipalPayment(BigDecimal.TEN, 1);

		assertEquals(0, result.compareTo(BigDecimal.TEN));
	}
}

@Getter
@RequiredArgsConstructor
class TestPaymentStep implements PaymentStep {

	private final PaymentType paymentType;
	private final BigDecimal interestRateForStep;
	private final BigDecimal extraPrincipalPayment;

}

@Getter
@RequiredArgsConstructor
class TestPaymentStepResult implements PaymentStepResult {

	@NotNull
	private final PaymentType paymentType;
	@NotNull
	private final BigDecimal interestRateForStep;
	@NotNull
	private final BigDecimal extraPrincipalPayment;
	@NotNull
	private final BigDecimal basePrincipalPayment;
	@NotNull
	private final BigDecimal interestPayment;
	@NotNull
	private final BigDecimal fullPayment;
	@NotNull
	private final BigDecimal principalAfterPayment;

}

@Getter
@RequiredArgsConstructor
class TestDebtPaymentSchedule implements DebtPaymentSchedule {

	@NotNull
	private final BigDecimal principal;
	@NotNull
	private final List<PaymentStep> paymentSteps;
}