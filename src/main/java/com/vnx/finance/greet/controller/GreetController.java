package com.vnx.finance.greet.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * added as a simple health check with predictable response
 */
@RestController
@RequestMapping("/greet")
public class GreetController {

	@GetMapping("/hello")
	public String hello() {

		return "hello";
	}
}
