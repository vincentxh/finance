package com.vnx.finance.debt;

import com.vnx.finance.debt.data.DebtPaymentOverview;
import com.vnx.finance.debt.data.implemented.DebtPaymentOverviewImpl;
import com.vnx.finance.debt.data.DebtPaymentSchedule;
import com.vnx.finance.debt.data.PaymentStep;
import com.vnx.finance.debt.data.PaymentStepResult;
import com.vnx.finance.debt.data.implemented.PaymentStepResultImpl;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.List;

public class StepPaymentService {

	/**
	 * Each loan is payed back in steps.
	 * This method returns an overview of the loan after each payment step.
	 * @param paymentSchedule
	 * @return
	 */
	public DebtPaymentOverview createDebtPaymentOverview(final DebtPaymentSchedule paymentSchedule) {

		final List<PaymentStepResult> result = new ArrayList<>();
		final var totalPaymentSteps = paymentSchedule.getPaymentSteps().size();

		var principal = paymentSchedule.getPrincipal();

		for (int currentPaymentStep = 0; currentPaymentStep < totalPaymentSteps; currentPaymentStep++) {

			final int paymentsLeft = totalPaymentSteps - currentPaymentStep;

			final PaymentStepResult paymentResult = buildPaymentStep(paymentSchedule.getPaymentSteps().get(currentPaymentStep),
																	 principal,
																	 paymentsLeft);

			result.add(paymentResult);

			principal = paymentResult.getPrincipalAfterPayment();
		}

		return new DebtPaymentOverviewImpl(paymentSchedule.getPrincipal(), result);
	}

	public PaymentStepResult buildPaymentStep(final PaymentStep currentPaymentStep, final BigDecimal principal,
			final int paymentsLeft) {

		/*
		 * calculate necessary components
		 */
		final var interestPayment = calculateInterestPayment(principal, currentPaymentStep.getInterestRateForStep());

		final var principalPayment = calculatePrincipalPayment(principal, paymentsLeft, currentPaymentStep);

		final var fullPayment = calculateFullPayment(principal, paymentsLeft, currentPaymentStep);

		/*
		 * reduce principal
		 */
		final var reducedPrincipal = principal.subtract(principalPayment)
											  .subtract(currentPaymentStep.getExtraPrincipalPayment());

		/*
		 * build paymentStepResult
		 */
		return PaymentStepResultImpl.builder()
									.paymentType(currentPaymentStep.getPaymentType())
									.interestRateForStep(currentPaymentStep.getInterestRateForStep())
									.extraPrincipalPayment(currentPaymentStep.getExtraPrincipalPayment())
									.basePrincipalPayment(principalPayment)
									.interestPayment(interestPayment)
									.fullPayment(fullPayment)
									.principalAfterPayment(reducedPrincipal)
									.build();
	}

	public BigDecimal calculateInterestPayment(@NotNull BigDecimal principal, @NotNull BigDecimal interestMultiplier) {

		return principal.multiply(interestMultiplier);
	}

	/**
	 * NOTE: do this over PaymentType axis or this axis?
	 */
	public BigDecimal calculateFullPayment(final BigDecimal principal, final int periodsLeft,
			final PaymentStep paymentStep) {

		switch (paymentStep.getPaymentType()) {

			case LINEAR: {
				return calculateLinearPrincipalPayment(principal, periodsLeft)
						.add(calculateInterestPayment(principal, paymentStep.getInterestRateForStep()))
						.add(paymentStep.getExtraPrincipalPayment());
			}

			case ANNUITY: {
				return calculateAnnuityPayment(principal, periodsLeft, paymentStep.getInterestRateForStep())
						.add(paymentStep.getExtraPrincipalPayment());
			}
		}

		throw new IllegalArgumentException("Payment type not supported");
	}

	/**
	 *
	 * @param principal
	 * @param periodsLeft
	 * @param paymentStep
	 * @return
	 */
	public BigDecimal calculatePrincipalPayment(final BigDecimal principal, final int periodsLeft, final PaymentStep paymentStep) {

		switch (paymentStep.getPaymentType()) {

			case LINEAR: {
				return calculateLinearPrincipalPayment(principal, periodsLeft);
			}

			case ANNUITY: {

				return calculateAnnuityPrincipalPayment(principal, periodsLeft,
														paymentStep.getInterestRateForStep());
			}
		}

		throw new IllegalArgumentException("Payment type not supported");
	}

	/**
	 * @param principal
	 * @param periodsLeft
	 * @param periodInterestRate
	 * @return result = ( periodInterestRate / ( 1 - ( (1 + periodInterestRate) ^ -periodsLeft ) * principal
	 */
	public BigDecimal calculateAnnuityPayment(final BigDecimal principal, final int periodsLeft,
			final BigDecimal periodInterestRate) {

		final var periodFactor = periodInterestRate.add(BigDecimal.ONE)
												   .pow(-periodsLeft, MathContext.DECIMAL128);
		final var periodDivisor = BigDecimal.ONE.subtract(periodFactor);

		return periodInterestRate.divide(periodDivisor, MathContext.DECIMAL128)
								 .multiply(principal);
	}

	/**
	 *
	 * calculates which part of the annuity payment reduces the principal
	 *
	 * @param principal
	 * @param periodsLeft
	 * @param periodMultiplier
	 * @return result = annuityPayment - interestPayment
	 */
	public BigDecimal calculateAnnuityPrincipalPayment(BigDecimal principal, int periodsLeft,
			BigDecimal periodMultiplier) {

			final var annuityPayment = calculateAnnuityPayment(principal, periodsLeft, periodMultiplier);

			final var meh = calculateInterestPayment(principal, periodMultiplier);

			final var result = annuityPayment.subtract(meh);

			return result;
	}

	public BigDecimal calculateLinearPrincipalPayment(@NotNull BigDecimal principal, @Min(1) int periodsLeft) {

		return principal.divide(BigDecimal.valueOf(periodsLeft), MathContext.DECIMAL128);
	}


}
