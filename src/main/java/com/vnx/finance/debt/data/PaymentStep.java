package com.vnx.finance.debt.data;

import javax.validation.constraints.NotNull;

import java.math.BigDecimal;

public interface PaymentStep {

	/**
	 * @return payment type determining the base payment of this step
	 */
	@NotNull
	PaymentType getPaymentType();

	/**
	 * @return multiplier is 1 + r, where r is the interest rate for this step
	 */
	@NotNull
	BigDecimal getInterestRateForStep();

	/**
	 * @return extra payment to reduce the debt
	 */
	@NotNull
	BigDecimal getExtraPrincipalPayment();

}

