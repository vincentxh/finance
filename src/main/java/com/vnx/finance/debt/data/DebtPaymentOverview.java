package com.vnx.finance.debt.data;

import java.math.BigDecimal;
import java.util.List;

public interface DebtPaymentOverview {
	BigDecimal getPrincipal();
	List<PaymentStepResult> getPaymentOverview();
}
