package com.vnx.finance.debt.data.implemented;

import com.vnx.finance.debt.data.DebtPaymentOverview;
import com.vnx.finance.debt.data.PaymentStepResult;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Getter
@RequiredArgsConstructor
public class DebtPaymentOverviewImpl implements DebtPaymentOverview {

	private final BigDecimal principal;
	private final List<PaymentStepResult> paymentOverview;

}
