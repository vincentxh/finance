package com.vnx.finance.debt.data.implemented;

import com.vnx.finance.debt.data.PaymentStepResult;
import com.vnx.finance.debt.data.PaymentType;
import lombok.Builder;
import lombok.Getter;

import javax.validation.constraints.NotNull;

import java.math.BigDecimal;

@Getter
@Builder
public class PaymentStepResultImpl implements PaymentStepResult {

	@NotNull
	private final PaymentType paymentType;
	@NotNull
	private final BigDecimal interestRateForStep;
	@NotNull
	private final BigDecimal extraPrincipalPayment;
	@NotNull
	private final BigDecimal basePrincipalPayment;
	@NotNull
	private final BigDecimal interestPayment;
	@NotNull
	private final BigDecimal fullPayment;
	@NotNull
	private final BigDecimal principalAfterPayment;

}
