package com.vnx.finance.debt.data;

import javax.validation.constraints.NotNull;

import java.math.BigDecimal;
import java.util.List;

public interface DebtPaymentSchedule {

	/**
	 * @return principal (mortgage debt)
	 */
	@NotNull
	BigDecimal getPrincipal();

	/**
	 * @return all subsequent planned payment steps of equal length (e.g. month or year)
	 */
	@NotNull
	List<PaymentStep> getPaymentSteps();

}
