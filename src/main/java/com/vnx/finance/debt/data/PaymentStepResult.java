package com.vnx.finance.debt.data;

import javax.validation.constraints.NotNull;

import java.math.BigDecimal;

public interface PaymentStepResult {

	/**
	 * @return payment type determining the base payment of this step
	 */
	@NotNull
	PaymentType getPaymentType();

	/**
	 * @return the interest rate for this step
	 */
	@NotNull
	BigDecimal getInterestRateForStep();

	/**
	 * @return extra payment to reduce the debt
	 */
	@NotNull
	BigDecimal getExtraPrincipalPayment();

	@NotNull
	BigDecimal getBasePrincipalPayment();

	@NotNull
	BigDecimal getInterestPayment();

	/**
	 * @return previous balance - basePrincipalPayment - extraPrincipalPayment
	 */
	@NotNull
	BigDecimal getPrincipalAfterPayment();

	@NotNull
	BigDecimal getFullPayment();


}
