package com.vnx.finance.value.client.iex.data;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

/**
 *  GET /stock/{symbol}/quote
 */
@ToString
@Getter
@RequiredArgsConstructor
public class IEXStockQuoteDTO {

	private final String symbol;
	private final String companyName;
	// (previous day close price) / (ttmEPS)
	private final double peRatio;
	private final double previousClose;

}