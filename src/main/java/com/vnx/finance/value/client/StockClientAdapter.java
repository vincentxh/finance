package com.vnx.finance.value.client;

import com.vnx.finance.value.client.data.StockSummaryDTO;

public interface StockClientAdapter {

	StockSummaryDTO stockSummaryBy(final String symbol);

}
