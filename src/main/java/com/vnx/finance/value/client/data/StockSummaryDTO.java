package com.vnx.finance.value.client.data;

public interface StockSummaryDTO {

	String getSymbol();
	String getCompanyName();
	double getTrailingPE();
	double getPricePreviousClosing();
	double getDE();
	double getPS();
	double getTrailingDividendRate();

}
