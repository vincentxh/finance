package com.vnx.finance.value.client.iex.data;

import com.vnx.finance.value.client.data.StockSummaryDTO;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class IEXStockSummaryDTO implements StockSummaryDTO {

	private final String symbol;
	private final String companyName;
	private final double trailingPE;
	private final double pricePreviousClosing;
	private final double DE;
	private final double PS;
	private final double trailingDividendRate;

	public static IEXStockSummaryDTO from(final IEXStockQuoteDTO quote, final IEXStockAdvancedStatsDTO stats,
			final double trailingDividendRate) {

		return IEXStockSummaryDTO.builder()
								 .symbol(quote.getSymbol())
								 .companyName(quote.getCompanyName())
								 .trailingPE(quote.getPeRatio())
								 .pricePreviousClosing(quote.getPreviousClose())
								 .DE(stats.getDebtToEquity())
								 .PS(stats.getPriceToSales())
								 .trailingDividendRate(trailingDividendRate)
								 .build();
	}
}
