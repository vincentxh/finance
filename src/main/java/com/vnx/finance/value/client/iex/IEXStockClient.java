package com.vnx.finance.value.client.iex;

import com.vnx.finance.value.client.iex.data.IEXStockAdvancedStatsDTO;
import com.vnx.finance.value.client.iex.data.IEXStockQuoteDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;

/**
 * TODO: figure out how to disable application is client can not be reached
 */
@Component
public class IEXStockClient {

	private final RestTemplate valueTemplate;
	private final String apiRoot;
	private final String apiPublicKey;

	@Autowired
	public IEXStockClient(RestTemplate valueTemplate,
			@Value("${value.api.root}") final String apiRoot,
			@Value("${value.api.pkey}") final String apiPublicKey) {

		this.valueTemplate = valueTemplate;
		this.apiRoot = apiRoot;
		this.apiPublicKey = apiPublicKey;
	}

	@Async
	public CompletableFuture<Double> trailingDividendRateBy(String symbol) {

		final String endpoint = String.format("/stock/%s/stats/ttmDividendRate", symbol);

		return CompletableFuture.supplyAsync(() ->Optional.ofNullable(valueTemplate.getForObject(createIEX_URL(endpoint), Double.class))
					   .orElseThrow());
	}

	@Async
	public CompletableFuture<IEXStockQuoteDTO> quoteBy(final String symbol) {

		final String endpoint = String.format("/stock/%s/quote", symbol);

		final var target = createIEX_URL(endpoint);

		return CompletableFuture.supplyAsync(() -> valueTemplate.getForObject(target, IEXStockQuoteDTO.class));
	}

	/**
	 * TODO: Mock for now because it is only accessible paid
	 */
	@Async
	public CompletableFuture<IEXStockAdvancedStatsDTO> statsBy(final String symbol) {

		// activate when going to paid tier
//		final String endpoint = String.format("/stock/%s/advanced-stats", symbol);
//
//		return valueTemplate.getForObject(createIEX_URI(endpoint), IEXStockAdvancedStatsDTO.class);

		return CompletableFuture.supplyAsync(() -> new IEXStockAdvancedStatsDTO(42, 42));
	}

	private String createIEX_URL(final String endpoint) {

		return apiRoot + endpoint + "?token=" + apiPublicKey;
	}
}
