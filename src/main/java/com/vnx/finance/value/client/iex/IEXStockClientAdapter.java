package com.vnx.finance.value.client.iex;

import com.vnx.finance.value.client.StockClientAdapter;
import com.vnx.finance.value.client.data.StockSummaryDTO;
import com.vnx.finance.value.client.iex.data.IEXStockSummaryDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * TODO: figure out how to disable application is client can not be reached
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class IEXStockClientAdapter implements StockClientAdapter {

	private final IEXStockClient iexStockClient;

	@Override
	public StockSummaryDTO stockSummaryBy(String symbol) {

		final var quote = iexStockClient.quoteBy(symbol);
		final var stats = iexStockClient.statsBy(symbol);
		final var trailingDividendRate = iexStockClient.trailingDividendRateBy(symbol);

		CompletableFuture.allOf(quote, stats, trailingDividendRate);

		try {
			return IEXStockSummaryDTO.from(quote.get(), stats.get(), trailingDividendRate.get());
		} catch (final InterruptedException | ExecutionException e) {

			log.error(e.toString());

			throw new RuntimeException(e);
		}
	}

}
