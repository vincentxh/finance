package com.vnx.finance.value.service.data;

public interface StockSummary {

	String getSymbol();
	String getCompanyName();
	double getTrailingPE();
	double getPricePreviousClosing();
	double getDE();
	double getPS();
	double getTrailingDividendRate();

}
