package com.vnx.finance.value.service.data;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class StockSummaryModel implements StockSummary {

	private final String symbol;
	private final String companyName;
	private final double trailingPE;
	private final double pricePreviousClosing;
	private final double DE;
	private final double PS;
	private final double trailingDividendRate;

}
