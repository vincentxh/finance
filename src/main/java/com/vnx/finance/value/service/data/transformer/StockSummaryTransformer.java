package com.vnx.finance.value.service.data.transformer;

import com.vnx.finance.value.client.data.StockSummaryDTO;
import com.vnx.finance.value.service.data.StockSummary;
import com.vnx.finance.value.service.data.StockSummaryModel;
import org.springframework.stereotype.Component;

@Component
public class StockSummaryTransformer {

	public StockSummary stockSummaryFrom(final StockSummaryDTO dto) {

		return StockSummaryModel.builder()
								.symbol(dto.getSymbol())
								.companyName(dto.getCompanyName())
								.trailingPE(dto.getTrailingPE())
								.pricePreviousClosing(dto.getPricePreviousClosing())
								.DE(dto.getDE())
								.PS(dto.getPS())
								.trailingDividendRate(dto.getTrailingDividendRate())
								.build();
	}

}
