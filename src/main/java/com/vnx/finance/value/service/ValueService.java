package com.vnx.finance.value.service;

import com.vnx.finance.value.service.data.StockSummary;

public interface ValueService {

	StockSummary stockSummaryBySymbol(final String symbol);

}
