package com.vnx.finance.value.service.implementation;

import com.vnx.finance.value.client.StockClientAdapter;
import com.vnx.finance.value.service.ValueService;
import com.vnx.finance.value.service.data.StockSummary;
import com.vnx.finance.value.service.data.transformer.StockSummaryTransformer;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ValueServiceImpl implements ValueService {

	private final StockClientAdapter stockClientAdapter;
	private final StockSummaryTransformer stockSummaryTransformer;

	public StockSummary stockSummaryBySymbol(final String symbol) {

		final var stockSummaryDTO = stockClientAdapter.stockSummaryBy(symbol);

		return stockSummaryTransformer.stockSummaryFrom(stockSummaryDTO);
	}

}
