package com.vnx.finance.value.controller.data.outbound;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.UUID;

@Getter
@RequiredArgsConstructor
public class IndexOverviewRest {

	private final UUID id;
	private final String name;
	private final double lastClose;

}
