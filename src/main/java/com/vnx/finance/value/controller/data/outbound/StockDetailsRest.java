package com.vnx.finance.value.controller.data.outbound;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class StockDetailsRest {

//	GET /stock/{symbol}/company
	// TODO: decide what other profile info to add
	private final String description;

	// should be wiki style adjustable
	private final String investorRelationsSite;

}
