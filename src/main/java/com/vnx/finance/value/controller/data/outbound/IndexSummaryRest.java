package com.vnx.finance.value.controller.data.outbound;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.UUID;

@Getter
@RequiredArgsConstructor
public class IndexSummaryRest {

	private final UUID indexId;
	private final String name;
	private final String symbol;
}
