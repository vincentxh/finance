package com.vnx.finance.value.controller;

import com.vnx.finance.value.controller.data.outbound.IndexOverviewRest;
import com.vnx.finance.value.controller.data.outbound.IndexSummaryRest;
import com.vnx.finance.value.controller.data.outbound.StockSummaryRest;
import com.vnx.finance.value.service.ValueService;
import com.vnx.finance.value.service.data.StockSummary;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

/**
 * This will be the start of a value investing API. Should add a Trello  board to specify steps for development.
 */
@RestController
@RequestMapping({"api/v0/"})
@RequiredArgsConstructor
public class ValueController {

	private final ValueService valueService;

	/**
	 * should at least return the index name with the matching id
	 * @return
	 */
	@GetMapping("/index")
	public List<IndexSummaryRest> availableIndexes() {

		return List.of(new IndexSummaryRest(UUID.randomUUID(), "AEX", "AEX"), new IndexSummaryRest(UUID.randomUUID(),
																							 "NASDAQ", "NASDAQ"));
	}

	/**
	 *
	 * @return
	 */
	@GetMapping("/index/{id}")
	public IndexOverviewRest indexById(@PathVariable UUID id) {

		return new IndexOverviewRest(UUID.randomUUID(), "AEX", 42.0);
	}

	/**
	 *
	 * should return a list with id's for tickers per index
	 * @param id
	 * @return
	 */
	@GetMapping("/index/{id}/tickers")
	public List<UUID> tickersByIndexId(@PathVariable UUID id) {

		return List.of(UUID.randomUUID());
	}

	/**
	 *
	 * TODO: think to always add index or market, /market/AEX/stock/KLM
	 *
	 * should return a summary of a stock
	 * @param symbol
	 * @return
	 */
	@GetMapping("/stock/{symbol}")
	public StockSummaryRest stockById(@PathVariable String symbol) {

		final StockSummary summary = valueService.stockSummaryBySymbol(symbol);

		return StockSummaryRest.stockSummaryRestFrom(summary);
	}

	/**
	 * @return stock value data by week, or by month for an x period to build a graph in the frontend
	 * @GetMapping("/stock/data/{id}")
	 */

	/**
	 * @return details of company (e.g. profile)
	 * @GetMapping("/stock/details/{id})
	 */

	/**
	 * next iterations:
	 * ETF's
	 * Commodities
	 *
	 */
}
