package com.vnx.finance.value.controller.data.outbound;

import com.vnx.finance.value.service.data.StockSummary;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class StockSummaryRest {

	private final String symbol;
	private final String companyName;
	private final double trailingPE;
	private final double pricePreviousClosing;
	private final double DE;
	private final double PS;
	private final double trailingDividendRate;

	public static StockSummaryRest stockSummaryRestFrom(final StockSummary dto) {

		return StockSummaryRest.builder()
							   .symbol(dto.getSymbol())
							   .companyName(dto.getCompanyName())
							   .trailingPE(dto.getTrailingPE())
							   .pricePreviousClosing(dto.getPricePreviousClosing())
							   .DE(dto.getDE())
							   .PS(dto.getPS())
							   .trailingDividendRate(dto.getTrailingDividendRate())
							   .build();
	}
}
