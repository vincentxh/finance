package com.vnx.finance.compound.service;


import com.vnx.finance.compound.service.data.CompoundingSchedule;
import com.vnx.finance.compound.service.data.CompoundingStepResult;
import com.vnx.finance.compound.service.data.CompoundingStepResultImp;

import javax.validation.constraints.NotNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class StepCompounderService {

	/**
	 * @param schedule to which a capital should be compounded
	 * @return overview of compounded capital following the schedule
	 */
	@NotNull
	public List<CompoundingStepResult> compound(final @NotNull CompoundingSchedule schedule) {

		final List<CompoundingStepResult> compoundingStepResults = new ArrayList<>();

		compoundingStepResults.add(

				new CompoundingStepResultImp(BigDecimal.ONE,
											 schedule.getLumpSum(),
											 schedule.getLumpSum()
		));

		var compoundingCapital = schedule.getLumpSum();

		for(var compoundingStep: schedule.getCompoundingSteps()) {

			compoundingCapital = _depositAndCompound(compoundingCapital,
													 compoundingStep.getDepositForStep(),
													 compoundingStep.getCompoundingStepMultiplier());

			compoundingStepResults.add(
				new CompoundingStepResultImp(
					compoundingStep.getCompoundingStepMultiplier(),
					compoundingStep.getDepositForStep(),
					compoundingCapital
				)
			);
		}

		return compoundingStepResults;
	}

	/**
	 *
	 * Deposit to balance and increase with interest.
	 *
	 * @param previousBalance
	 * @param deposit
	 * @param multiplier = 1 + interest rate
	 * @return
	 */
	@NotNull
	public BigDecimal _depositAndCompound(@NotNull final BigDecimal previousBalance,
			@NotNull final BigDecimal deposit,
			@NotNull final BigDecimal multiplier)
	{
		final var newBalance = 	previousBalance.add(deposit)
										.multiply(multiplier);

		return newBalance;
	}

}