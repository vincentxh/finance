package com.vnx.finance.compound.service.data;

import javax.validation.constraints.NotNull;

import java.math.BigDecimal;

public interface CompoundingStep {

	@NotNull
	BigDecimal getCompoundingStepMultiplier();

	@NotNull
	BigDecimal getDepositForStep();

}
