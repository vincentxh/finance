package com.vnx.finance.compound.service.data;

import javax.validation.constraints.NotNull;

import java.math.BigDecimal;
import java.util.List;

/**
 * Represents a schedule to compound money over steps of time, each a {@link CompoundingStep}.
 *
 * At the start of compounding a lumpSum can be added as starting value.
 *
 * At each step a deposit is made and then multiplied by the multiplier for that step.
 *
 * The order in the {@link List} <{@link CompoundingStep}> is presumed to be
 * the order at which the capital should be compounded.
 */
public interface CompoundingSchedule {

	@NotNull
	BigDecimal getLumpSum();
	@NotNull
	List<CompoundingStep> getCompoundingSteps();

}
