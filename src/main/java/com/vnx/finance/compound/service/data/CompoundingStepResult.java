package com.vnx.finance.compound.service.data;

import javax.validation.constraints.NotNull;

import java.math.BigDecimal;

/**
 * Represents a snapshot of a balance after a deposit has been added and then increased by interest.
 */
public interface CompoundingStepResult {

	@NotNull
	BigDecimal getCompoundingStepMultiplier();

	@NotNull
	BigDecimal getDepositForStep();

	@NotNull
	BigDecimal getBalanceAfterStep();
}
