package com.vnx.finance.compound.service.data;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.NotNull;

import java.math.BigDecimal;

@Getter
@RequiredArgsConstructor
public class CompoundingStepResultImp implements CompoundingStepResult {

	@NotNull
	private final BigDecimal compoundingStepMultiplier;
	@NotNull
	private final BigDecimal depositForStep;
	@NotNull
	private final BigDecimal balanceAfterStep;

}
