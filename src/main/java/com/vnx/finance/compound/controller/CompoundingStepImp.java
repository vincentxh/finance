package com.vnx.finance.compound.controller;

import com.vnx.finance.compound.service.data.CompoundingStep;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.NotNull;

import java.math.BigDecimal;

@Getter
@RequiredArgsConstructor
public class CompoundingStepImp implements CompoundingStep {

	@NotNull
	private final BigDecimal compoundingStepMultiplier;
	@NotNull
	private final BigDecimal depositForStep;

}
