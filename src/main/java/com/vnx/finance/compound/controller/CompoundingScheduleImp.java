package com.vnx.finance.compound.controller;

import com.vnx.finance.compound.service.data.CompoundingSchedule;
import com.vnx.finance.compound.service.data.CompoundingStep;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Getter
@RequiredArgsConstructor
class CompoundingScheduleImp implements CompoundingSchedule {

	private final BigDecimal lumpSum;
	private final List<CompoundingStep> compoundingSteps;

}
