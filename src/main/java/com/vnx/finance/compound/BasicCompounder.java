package com.vnx.finance.compound;

import com.vnx.finance.util.BigDecimalValidationUtil;

import java.math.BigDecimal;
import java.math.MathContext;

/**
 * based on https://financieel.infonu.nl/sparen/98100-hoeveel-per-maand-sparen-om-een-zeker-bedrag-te-verkrijgen.html#:~:text=Kapitaalgroei%20met%20maandelijkse%20inleg,-Stel%20je%20voor&text=Dan%20maak%20je%20handig%20gebruik,S%20%3D%20de%20inleg%20per%20maand%3B&text=i%20%3D%20het%20aantal%20maanden%20dat%20je%20gaat%20inleggen.
 *
 * Notes: these are actually just shortcuts to lump up the compounding
 *
 */
public class BasicCompounder {

	/**
	 * This method will calculate a capital compounded at a fixed rate with a possible monthly deposit

	 *
	 * @param steps
	 * @param stepMultiplier is 1 + interestRate per step
	 * @param lumpSum
	 * @param depositPerStep is the additional deposit per step at the end of each month
	 * @return total = compounded lumpsum + compounded deposits
	 */
	public BigDecimal calculateCompoundedCapital(final int steps, final BigDecimal stepMultiplier,
			final BigDecimal lumpSum,
			final BigDecimal depositPerStep) {

		assert(steps >= 0);
		BigDecimalValidationUtil.validateBigDecimalToDoubleReliability(stepMultiplier);
		BigDecimalValidationUtil.validateBigDecimalToDoubleReliability(lumpSum);
		BigDecimalValidationUtil.validateBigDecimalToDoubleReliability(depositPerStep);

		final var lumpSumResult = calculateCompoundedLumpSum(steps, stepMultiplier, lumpSum);

		final var depositsResult = calculateCompoundedDeposits(steps, stepMultiplier, depositPerStep);

		return lumpSumResult.add(depositsResult);
	}

	/**
	 *
	 * lumpsum_total = B * multiplier ^ i
	 *
	 * @param step
	 * @param stepMultiplier
	 * @param startCapital
	 * @return
	 */
	public BigDecimal calculateCompoundedLumpSum(int step, BigDecimal stepMultiplier, BigDecimal startCapital) {

		return startCapital.multiply(stepMultiplier.pow(step));
	}

	/**
	 *
	 * multiplier = step rate + 1
	 * i = periods to save
	 * deposit_total = S * (multiplier ^ i) - 1) * ( multiplier ) / (multiplier - 1)
	 *
	 * @param step
	 * @param stepMultiplier
	 * @param capitalDepositPerStep
	 * @return
	 */

	public BigDecimal calculateCompoundedDeposits(int step, BigDecimal stepMultiplier, BigDecimal capitalDepositPerStep) {

		final var first = stepMultiplier.pow(step)
										.subtract(BigDecimal.ONE);

		final var second = 	stepMultiplier.divide((stepMultiplier.subtract(BigDecimal.ONE)), MathContext.DECIMAL128);

		return capitalDepositPerStep.multiply(first)
									.multiply(second);
	}

}
