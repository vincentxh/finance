package com.vnx.finance.util;

import java.math.BigDecimal;

public class BigDecimalValidationUtil {


	public static void validateBigDecimalToDoubleReliability(final BigDecimal check) {

		final double test = check.doubleValue();
		if (test == Double.NEGATIVE_INFINITY || test == Double.POSITIVE_INFINITY) {
			throw new ArithmeticException("Yearly rate to large to calculate monthly rate");
		}
	}
}
