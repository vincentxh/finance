package com.vnx.finance.util;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import java.math.BigDecimal;
import java.math.MathContext;
import java.time.Month;

public class InterestUtil {

	/**
	 *
	 * yearlyRateOfIncrease = r
	 * result = (1 + r ) ^ (1/12)
	 * 1 + yearlyRateOfIncrease = result ^ 12
	 *
	 * NOTE: due to fractional powers, a slight loss of information occurs. This loss is nog larger than 10^-7
	 *
	 * @param yearlyRateOfIncrease as  (e.g. 0.05 =
	 * @return monthly multiplication rate = 1 + monthly rate of increase
	 */
	public static BigDecimal monthlyMultiplier(@NotNull final BigDecimal yearlyRateOfIncrease) {

		return calculateSubPeriodMultiplier(yearlyRateOfIncrease, Month.values().length);
	}

	/**
	 * @param yearlyRateOfIncrease
	 * @return monthly rate of increase
	 */
	public static BigDecimal monthlyInterestRate(@NotNull final BigDecimal yearlyRateOfIncrease) {

		return calculateSubPeriodInterestRate(yearlyRateOfIncrease, Month.values().length);
	}

	/**
	 * abstracted {@link InterestUtil#monthlyMultiplier(BigDecimal)}
	 *
	 * periodicRateOfIncrease = r
	 * subPeriods = p
	 *
	 * @return result = (1 + r ) ^ (1/p)
	 *
	 * where 1 + periodicRateOfIncrease = result ^ p
	 *
	 */
	public static BigDecimal calculateSubPeriodMultiplier(@NotNull final BigDecimal periodicRateOfIncrease,
			@Min(1) final int subPeriods) {

		BigDecimalValidationUtil.validateBigDecimalToDoubleReliability(periodicRateOfIncrease);

		final BigDecimal periodicIncreaseFactor = BigDecimal.ONE.add(periodicRateOfIncrease);

		/*
		 * 1/subPeriod can be an infinite decimal, loss of information can be limited with a finer MatchContext
		 */
		final BigDecimal powFactor = BigDecimal.ONE.divide(BigDecimal.valueOf(subPeriods), MathContext.DECIMAL128);

		/*
		 * The biggest loss of information occurs here, but is inevitable due to fractional power.
		 */
		return BigDecimal.valueOf(Math.pow(periodicIncreaseFactor.doubleValue(), powFactor.doubleValue()));
	}
	/**
	 *
	 * The method was added to calculate the interest rate only.
	 * In the case of loans, the interest rate is needed to calculate each periodic payment.
	 *
	 * @return {@link InterestUtil#calculateSubPeriodMultiplier(BigDecimal, int)} - 1
	 *
	 */
	public static BigDecimal calculateSubPeriodInterestRate(@NotNull final BigDecimal periodicRateOfIncrease,
			@Min(1) final int subPeriods) {

		return calculateSubPeriodMultiplier(periodicRateOfIncrease, subPeriods).subtract(BigDecimal.ONE);
	}
}
