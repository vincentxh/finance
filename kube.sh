# borrowed from https://cloud.google.com/kubernetes-engine/docs/tutorials/hello-app#cloud-shell

mvn package -DskipTests

export PROJECT_ID=organic-isotope-281313
docker build -t gcr.io/${PROJECT_ID}/finance-service:v0 .

docker images

# gcloud auth configure-docker
docker push gcr.io/${PROJECT_ID}/finance-service:v0

gcloud config set project $PROJECT_ID
gcloud config set compute/zone europe-west4-a
gcloud container clusters create finance-service-cluster

gcloud compute instances list

kubectl create deployment finance-service-deployment --image=gcr.io/${PROJECT_ID}/finance-service:v0
kubectl scale deployment finance-service-deployment --replicas=3
kubectl autoscale deployment finance-service-deployment --cpu-percent=80 --min=1 --max=5

kubectl get pods

kubectl expose deployment finance-service-deployment --name=finance-service --type=LoadBalancer --port 80 --target-port 8080

kubectl get service

gcloud container clusters resize finance-service-cluster --size 1
# kubectl delete service finance-service
# gcloud container clusters delete finance-service-cluster